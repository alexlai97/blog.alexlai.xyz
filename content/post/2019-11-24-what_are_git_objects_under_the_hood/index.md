---
title : "What are git objects under the hood"
date : 2019-11-24
lastmod: 2020-04-08
draft: false
keywords: []
categories : ["explanation"]
tags : ["blog", "git", "explanation", "advanced"]
summary: "Have you ever looked at what's inside `.git/` ?"
asciinema: true

---

> "git - the stupid content tracker" – description of `man git` written by Linus Torvalds

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->

- [What are under the hood ?](#what-are-under-the-hood-)
    - [git objects and some "pointers"](#git-objects-and-some-pointers)
    - [decompress and compress the object](#decompress-and-compress-the-object)
    - [git cat-file](#git-cat-file)
    - [inside tag, commit, tree, blob objects](#inside-tag-commit-tree-blob-objects)
    - [blob object](#blob-object)
    - [tree object](#tree-object)
    - [commit object](#commit-object)
    - [tag object](#tag-object)
- [Tools to interact with git](#tools-to-interact-with-git)
- [references and resources](#references-and-resources)

<!-- markdown-toc end -->


# What are under the hood ?

## git objects and some "pointers"
![git basic objects relations](./git_basic_objects_relations.png)

- the ones in oval are objects
- the ones in rectangles are pointers

- HEAD points to the branch or commit where you are on
- remote, ([annotated](https://stackoverflow.com/questions/11514075/what-is-the-difference-between-an-annotated-and-unannotated-tag#11514139)) tag, branch points to a commit
- a commit points to another commit (its parent if any) and a tree
- a tree could point to multiple blobs and trees


## decompress and compress the object

How to see what's inside a git object ? 

[demo of decompress and compress using zlib and sha1](https://asciinema.org/a/DAkrzbtGiu7UqoNouJvMnyuSa)

{{< asciinema cast_path="git_objects_under_the_hood/decompress_compress_using_zlib_and_sha1.cast" cols="133" rows="39" theme="asciinema">}}


You can use `decompress` the filename to decompress the git object using [`zlib`](https://en.wikipedia.org/wiki/Zlib) and `compress` the byte string to get the [`sha1`](https://en.wikipedia.org/wiki/SHA-1) hash of the object, which is very similar to its filename.

```python
### compress_lib.py
import zlib  # A compression / decompression library
def decompress(filename):
    compressed_contents = open(filename, 'rb').read()
    decompressed_contents = zlib.decompress(compressed_contents)
    return decompressed_contents

from hashlib import sha1  # SHA1 hash algorithm
def compress(decompressed_contents):
    return sha1(decompressed_contents).hexdigest()
```

```python
### interactive python
>>> content = decompress('./.git/objects/ac/505a3830f82c59ecbe11064e33987102c96de0') # input a filename of a git object
>>> content 
b'blob 1251\x00\n\nEos dicta neque ut. Ut doloremque ducimus et autem commodi non incidunt et. At dolore expedita et error in ipsum voluptas et. Amet voluptatibus in voluptas. Quidem nam harum porro earum enim impedit. Totam et consequatur amet veniam cupiditate magni.\n\nDebitis magni repellendus sunt quos et. Sapiente voluptatem ut eaque quas beatae nobis facere. Qui a eveniet recusandae placeat omnis esse. Accusantium provident sit quia voluptatem. Voluptatum mollitia consequatur omnis dignissimos suscipit.\n\nEx fuga ad nam voluptatibus sapiente nobis aspernatur. Quasi fugit culpa libero veniam dolorem et dolorem quidem. Aut ratione hic magni. Dolor nam architecto at nihil. Sunt odio temporibus voluptatem et et atque labore. Vel delectus atque sed et.\n\nAut perferendis fugit exercitationem aut praesentium labore itaque facilis. Optio et ea error soluta sunt quia deleniti. Dignissimos aspernatur molestias tenetur hic debitis. Mollitia quisquam molestiae et doloribus. Facere culpa veniam minima. Autem vel dolor molestiae quia.\n\nQuis dolores quis a molestiae. Aspernatur laudantium at animi. Suscipit aut accusantium et delectus excepturi maxime. Aut cupiditate eos ab id possimus suscipit. Et enim consequatur hic culpa facilis. Fuga molestias quis aut.\n\n'
>>> compress(content) # input the byte string and get the sha1 
'ac505a3830f82c59ecbe11064e33987102c96de0' # very similar to the filename
```

So 
- a git object could be decompressed using a compression library such as [`zlib`](https://en.wikipedia.org/wiki/Zlib)
- a git object is stored in such a manner that the concatenation of the its directory name (2 chars `'ac'`) and the file name (38 chars `'505a3830f82c59ecbe11064e33987102c96de0'`) is actually the [`sha1`](https://en.wikipedia.org/wiki/Sha1) hash of the content with the object type (`blob`) and number of bytes (`1251`) included shown in `content` variable above

## git cat-file

Instead of using the script above, `git cat-file` ([man](https://linux.die.net/man/1/git-cat-file)) is a builtin way to print the content of a git object. 

[demo of git cat-file](https://asciinema.org/a/cuQdmq5JnfcakExw9YYrEiXxl)

{{< asciinema cast_path="git_objects_under_the_hood/git_cat_file_demo.cast" cols="133" rows="39" theme="asciinema">}}

- `git cat-file -t` is to print the type of the object
- `git cat-file -p` is to pretty print the content of the object
- `man git-cat-file` for more 

I will use `git cat-file -p` for the rest of the article so you won't see the type of the object or the size of the object in bytes that you can see by decompress using python script above. 

And I will **omit them** when describing the content of a git object for the rest of the article.

## inside tag, commit, tree, blob objects

What are inside the tag object, commit object, tree object, blob object ?

Let's take a look at them. 

[demo of inside tag, commit, tree, blob](https://asciinema.org/a/ruaTNbFFh4QunRUKUQaAyw1Z2)

{{< asciinema cast_path="git_objects_under_the_hood/inside_tag_commit_tree_blob_objects.cast" cols="133" rows="38" theme="asciinema">}}

```fish
### a summary of demo
$ tree # show current directory (omit hidden files)
.
├── a.txt
└── directory
    ├── b.txt
    ├── c.txt
    └── d.txt 

$ cat .git/refs/tags/annotated_tag # annotated_tag is an object with hash below
f2b8234deeb7712edf24c03e8c700714ee1863d0

$ git cat-file -p f2b82 # what's inside the tag object
object fad735b6d6b1d5b90abfc97f3b332363c26bdf24
type commit
tag annotated_tag                              
tagger Alex Lai <contactme@alexlai.xyz> 1574626019 -0500

Yes, a annotated tag

$ git cat-file -p fad7 # what's inside the commit object
tree cf3ee3a86b13332bcc01f62b53cdcffe91b42785
author Alex Lai <contactme@alexlai.xyz> 1574626016 -0500
committer Alex Lai <contactme@alexlai.xyz> 1574626016 -0500

commit

$ git cat-file -p cf3ee # what's inside the tree object
100644 blob 78981922613b2afb6025042ff6bd878ac1994e85    a.txt
040000 tree 2edf42f04dba21ca928fdc9456b717bb0c6cf80f    directory

$ git cat-file -p 78981 # what's inside a blob object
a

$ git cat-file -p 2edf # what's inside another tree object
100644 blob 61780798228d17af2d34fce4cfbdf35556832472    b.txt
100644 blob f2ad6c76f0115a6ba5b00456a849810e7ec0af20    c.txt
100644 blob 4bcfe98e640c8284511312660fb8709b0afa888e    d.txt
```

Now we know exactly what are in these objects, let's examine them one by one.


## blob object
```fish
$ git cat-file -p 78981 # what's inside a blob object
a  # ← file content
```

A blob stores the content of a file but not the filename nor the [mode](https://en.wikipedia.org/wiki/Modes_(Unix)).

What do you think will happen to the `.git/objects/` directory if we create 100 files with same content ?

[demo of creating 100 same files](https://asciinema.org/a/DWUqj1ODLaxxLr1BCQ9F7MJoa)

{{< asciinema cast_path="git_objects_under_the_hood/create_100_same_files.cast" cols="133" rows="39" theme="asciinema">}}

Answer: only one blob object is created.

## tree object
```fish
$ git cat-file -p cf3ee # what's inside the tree object
100644 blob 78981922613b2afb6025042ff6bd878ac1994e85    a.txt
040000 tree 2edf42f04dba21ca928fdc9456b717bb0c6cf80f    directory
# ↑     ↑                 ↑                               ↑
#mode  type           sha1 hash                         filename

$ git cat-file -p 2edf # what's inside another tree object
100644 blob 61780798228d17af2d34fce4cfbdf35556832472    b.txt
100644 blob f2ad6c76f0115a6ba5b00456a849810e7ec0af20    c.txt
100644 blob 4bcfe98e640c8284511312660fb8709b0afa888e    d.txt
```
A tree stores a list of hashes of trees and blobs with the names and [mode](https://en.wikipedia.org/wiki/Modes_(Unix))s of those trees and blobs.

It is like a directory.

## commit object

```fish
$ git cat-file -p fad7 # what's inside the commit object
tree cf3ee3a86b13332bcc01f62b53cdcffe91b42785    # ← sha1 hash of the tree object
author Alex Lai <contactme@alexlai.xyz> 1574626016 -0500
committer Alex Lai <contactme@alexlai.xyz> 1574626016 -0500
                                      #     ↑           ↑
                                      #    time      timezone
commit  # ← comment/message
```
A commit stores the hash of a tree and author, committer, time, message, and the hash of its parent commit. 

The first commit doesn't have a parent.

An example of a commit with parent.

```fish
$ git log --oneline
6ca1704 (HEAD -> master) child
7aef601 parent

$ git cat-file -p 6ca1
tree 4b825dc642cb6eb9a060e54bf8d69288fbee4904     # ← tree hash
parent 7aef6017a128d5c07bb71fff61a75f996c5c36f6   # ← parent commit hash
author Alex Lai <contactme@alexlai.xyz> 1574701198 -0500
committer Alex Lai <contactme@alexlai.xyz> 1574701198 -0500

child # ← comment/message
```

Let's take a look at an example ([rust repository](https://github.com/rust-lang/rust)) in the wild. 

[demo of rust commits](https://asciinema.org/a/fsKYtvn2oMbXoRg2mlYQYboR7)
{{< asciinema cast_path="git_objects_under_the_hood/commits_of_rust.cast" cols="133" rows="38" theme="asciinema">}}

You can think of commits as a [forest](http://mathworld.wolfram.com/Forest.html) where each child stores the hash of its parent (or it will be an abandoned child).

You can imagine how messy the commits could be. 

## tag object

```fish
$ git cat-file -p f2b82 # what's inside the tag object
object fad735b6d6b1d5b90abfc97f3b332363c26bdf24 # ← object hash
type commit  # ← object type
tag annotated_tag # ← tag name
tagger Alex Lai <contactme@alexlai.xyz> 1574626019 -0500

Yes, a annotated tag  # ← comment/message
```

A tag (in this case it is an [annotated tag](https://stackoverflow.com/questions/11514075/what-is-the-difference-between-an-annotated-and-unannotated-tag#11514139)) stores object, type, tag, tagger and a messenge.

It provides a permanent shorthand name for a particular commit while a branch "pointer" can move around.

Let's look at tags still in the [rust](https://github.com/rust-lang/rust) example. 

[demo of rust tag 1.39.0](https://asciinema.org/a/22OtLQi8wBiLD7XYLR38snz3B)

{{< asciinema cast_path="git_objects_under_the_hood/rust_tag_1.39.0.cast" cols="133" rows="38" theme="asciinema">}}

The tag (or commit) can also be signed by your pgp secret key, ensuring the cryptographic integrity to a release or a version.

# Tools to interact with git
Here are a list of tools I often use to interact with git:
- `git` commands in shell, which I feel safe most
- [vim-fugitive](https://github.com/tpope/vim-fugitive) (very powerful and interactive plugin, can replace the `git` commands in shell by `:Git` functions in vim) 
- [emacs magit](https://github.com/tpope/vim-fugitive) I am still new to this, it's very interactive.

Here are some more gui tools I have tried:
- [gitk](https://git-scm.com/docs/gitk) not bad, I use it sometimes to visualize
- [git gui](https://git-scm.com/docs/git-gui) well, I already have vim-fugitive
- [gitkraken](https://www.gitkraken.com/) (very good but it's [not free](https://www.gitkraken.com/pricing#plan-comparison) for private repositories)

# references and resources
- youtube ["Git Internals - How Git Works - Fear Not The SHA!"](https://youtu.be/P6jD966jzlk)
- youtube ["Advanced Git: Graphs, Hashes, and Compression, Oh My!"](https://youtu.be/ig5E8CcdM9g)
- pdf: [Git Internals PDF](https://github.com/pluralsight/git-internals-pdf)
- website: ["Reading git objects" on Curious git](https://matthew-brett.github.io/curious-git/reading_git_objects.html)
- hash: [`sha1`](https://en.wikipedia.org/wiki/SHA-1)
- compress library: [zlib](https://en.wikipedia.org/wiki/SHA-1)
- command: [`bat`](https://github.com/sharkdp/bat.git)
- man page: [`watch`](https://linux.die.net/man/1/watch)
- man page: [`git`](https://linux.die.net/man/1/git)
- shell: [`fish`](https://fishshell.com/)

