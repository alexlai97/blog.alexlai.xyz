---
title: "story about security E1: public key ↔ secret key, encrypt ↔ decrypt"
date: 2020-04-08
lastmod: 2020-04-08
draft: false
toc: false
keywords: []
description: "how to setup this blog"
tags: ["blog", "comic", "security", "story"]
categories: ["security"]
summary: "What is public v.s. private keys, encryption v.s. decryption ? Here's a story."

---
> One day

(_keep scrolling_)

.

.

.

.

.

.


![01](comics/01_alice_bored.png)
> Alice stayed at home for a long time and got bored.

.

.

.

.

.

.

![02](comics/02_alice_generate_keys.png)
> Alice generated a pair of keys (public key and private key). Alice was less bored.

.

.

.

.

.

.

![03](comics/03_alice_meet_bob.png)
> One day, Alice encountred Bob. Through each other's eyes, they knew they feel same inside.

.

.

.

.

.

.

.

![04](comics/04_too_many_people.png)
> However, there were so many people around Alice and Bob. They could not chat freely, all they wanted was just to enjoy the time between them. But it was hard. After staring each other for a while, Alice and Bob both seemed to remember something and they went home. 

.

.

.

.

.

.

.

![05](comics/05_go_home_grab_public_key.png)
> They went home, grabbed a copy of the their public key. 

.

.

.

.

.

.

.

![06](comics/06_exchange_public_keys.png)
> Alice: "Yo, wanna switch keys ?"  
> Bob: "Sure !!"  

.

.

.

.

.

.

.

![07](comics/07_bring_each_other_s_public_key_home.png)
> They brought the copy of each other's public key home. Now Alice has Bob's public key. Bob has Alice's public key.

.

.

.

.

.

.

.

![08](comics/08_alice_compose_encrypted_message.png)
> Alice wrote something using Bob's public key. It turned out to be non-human-readable. What did she write ?  
> Alice knew that only Bob could understand what she wrote because Bob has the other pair of the key (private key).

.

.

.

.

.

.

.

![09](comics/09_encrypted_message_delivering.png)
> The non-human-readable message was being deliverred. It might take a while.

.

.

.

.

.

.

.

![10](comics/10_bob_sees_encrypted_message.png)
> Bob: "What is this non-human-readable message?"  

.

.

.

.

.

.

.

![11](comics/11_bob_try_to_decrypt.png)
> Bob realized that he has the private key so he could decrypte the message.

.

.

.

.

.

.

.

![12](comics/12_decrypted_message.png)

.

.

.

.

.

.

.

![13](comics/13_the_end.png)

.

.

.

.

.

.

.

But how was Bob sure that the message was from Alice ?  
The Alice Bob knew was not like that. (well, they only met once)  
It was possible that someone tampered the message.  
That must be it!

What indeed happened ?

.

.

.

.

next episode(incoming): "story about security E2: sign ↔ verify"
 
