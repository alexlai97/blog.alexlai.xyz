---
title : "My linux tool box"
date: 2019-09-27
lastmod: 2020-04-08
draft: false
keywords: []
description: "my linux tool box"
categories : ["tools"]
tags : ["blog", "linux", "tools"]
summary: "simply a dump of applications I've used or am using. TL;DR -> [dotfiles](https://github.com/alexlai97/dotfiles)"

---

This article is simply a dump of applications I've used or am using. 

There are good videos on youtube (e.g. [luke smith](https://lukesmith.xyz/), [distrotube](https://distrotube.com/)) and good articles online (e.g. [archwiki](https://wiki.archlinux.org/)) so I won't go into the details and features of each application here.

The order of applications simply shows the tendency of my personal perferences. 
It does not mean one is better than another.
The rightmost ones are the ones I used most recently and vice versa.

The starred\* ones are my favorite(s).

Here is my [dotfiles](https://github.com/alexlai97/dotfiles).

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->

- [Linux Distribution](#linux-distribution)
- [Window Manager](#window-manager)
- [Text Editor](#text-editor)
    - [Neovim/vim plugins](#neovimvim-plugins)
    - [Other vim related stuff](#other-vim-related-stuff)
- [Shell](#shell)
- [Terminal Emulator](#terminal-emulator)
- [Email client](#email-client)
- [File managers](#file-managers)
- [pdf viewer](#pdf-viewer)
- [Minimalism](#minimalism)
- [Rusty](#rusty)
- [Terminal based](#terminal-based)
- [More resources](#more-resources)

<!-- markdown-toc end -->



# Linux Distribution
[mint](https://linuxmint.com/) -> [openSUSE](https://www.opensuse.org/) -> [ubuntu](https://ubuntu.com/) -> [kubuntu](https://kubuntu.org/) -> [arch](https://wiki.archlinux.org/) -> [gentoo](https://gentoo.org/)\* -> [arch](https://wiki.archlinux.org/)\*

# Window Manager
[i3](https://i3wm.org/)\* -> [dwm](https://dwm.suckless.org/)\* -> [awesomewm](https://awesomewm.org/)\*

# Text Editor 
[vim](https://www.vim.org/) -> [neovim](https://github.com/neovim/neovim) -> [spacemacs](http://spacemacs.org/) -> [spacevim](https://spacevim.org/) -> [neovim](https://github.com/neovim/neovim)\* -> [doom-emacs](https://github.com/hlissner/doom-emacs)\*

## Neovim/vim plugins
* [dein](https://github.com/Shougo/dein.vim): plugin manager
* [tagbar](https://github.com/majutsushi/tagbar): displays tags in a window
* [vim-fugitive](https://github.com/tpope/vim-fugitive): git wrapper
* [quick-scope](https://github.com/unblevable/quick-scope)
* [auto-pairs](https://github.com/jiangmiao/auto-pairs)
* [vim-which-key](https://github.com/liuchengxu/vim-which-key)
* [vim-commentary](https://github.com/tpope/vim-commentary)
* [vim-multiple-cursors](https://github.com/terryma/vim-multiple-cursors)
* [vim-startify](https://github.com/mhinz/vim-startify)
* [vim-surround](https://github.com/tpope/vim-surround)
* [coc.nvim](https://github.com/neoclide/coc.nvim)
* ... more checkout [my config inside dein block](https://github.com/alexlai97/dotfiles/blob/master/vim/.vim/config/setup/dein_packages_setup.vim#L26)

## Other vim related stuff
* [fish](https://fishshell.com/) shell vim mode: enable by this function `fish_vi_keybindings`
* firefox [Vim Vixen](https://github.com/ueokande/vim-vixen)

# Shell
[bash](https://www.gnu.org/software/bash/) -> [zsh](https://github.com/robbyrussell/oh-my-zsh) -> [ion](https://github.com/redox-os/ion) -> [fish](https://fishshell.com/)\*

more shells: [archwiki](https://wiki.archlinux.org/index.php/Command-line_shell)

# Terminal Emulator

[konsole](https://konsole.kde.org/) -> [xfce terminal](https://docs.xfce.org/apps/terminal/start) -> [xterm](https://invisible-island.net/xterm/) -> [urxvt](http://software.schmorp.de/pkg/rxvt-unicode.html) -> [st](http://st.suckless.org/)\* -> [termite](https://github.com/thestinger/termite) -> [alacritty](https://github.com/alacritty/alacritty)\*

more terminal emulators: [archwiki](https://wiki.archlinux.org/index.php/List_of_applications/Utilities#Terminal_emulators)

# Email client

* graphical: [thunderbird](https://www.thunderbird.net/en-US/)
* terminal: [neomutt](https://github.com/neomutt/neomutt)\*

tried notmuch in emacs, and watched videos about mu4e in emacs but I think I will stick with neomutt for now.

more email clients: [archwiki](https://wiki.archlinux.org/index.php/Category:Email_clients)

# File managers

* graphical: [natilus](https://wiki.gnome.org/Apps/Files) -> [dolphin](https://userbase.kde.org/Dolphin) -> [spacefm](http://ignorantguru.github.io/spacefm/) -> [thunar](https://docs.xfce.org/xfce/thunar/start)\*
* terminal: [ranger](https://ranger.github.io/)\* -> [vifm](https://vifm.info/)\*

navigating in shell is also not bad ...

more file managers: [archwiki](https://wiki.archlinux.org/index.php/List_of_applications/Utilities#File_managers)

# pdf viewer

[okular](https://okular.kde.org/) -> [evince](https://wiki.gnome.org/Apps/Evince) -> [zathura](https://pwmt.org/projects/zathura/)\* | [emacs](https://www.gnu.org/software/emacs/)

more document viewers: [archwiki](https://wiki.archlinux.org/index.php/PDF,_PS_and_DjVu)

# Minimalism
* [suckless tools](https://github.com/alexlai97/suckless_stuff)
* [zathura](https://git.pwmt.org/pwmt/zathura)
* [vimb](https://github.com/fanglingsu/vimb)

# Rusty
written in [rust](https://www.rust-lang.org/)
* [exa](https://github.com/ogham/exa): `ls` alternative
* [bat](https://github.com/sharkdp/bat)\*: `cat` alternative
* [starship](https://github.com/starship/starship)\*: shell prompt
* [ripgrep](https://github.com/BurntSushi/ripgrep): `grep` alternative
* [fd](https://github.com/sharkdp/fd): `find` alternative
* [skim](https://github.com/lotabout/skim): `fzf` alternative
* [mdbook](https://github.com/rust-lang-nursery/mdBook): create book from markdown files

# Terminal based
- [tuir](https://gitlab.com/ajak/tuir/): reddit in terminal
- [newsboat](https://newsboat.org/): rss reader
- [ncmpcpp](https://wiki.archlinux.org/index.php/Ncmpcpp): music player daemon client
- [neomutt](https://neomutt.org/): email client

# More resources
- [suckless](http://suckless.org/)
- [archwiki: List of applications](https://wiki.archlinux.org/index.php/List_of_applications)
- my [dotfiles](https://github.com/alexlai97/dotfiles)
