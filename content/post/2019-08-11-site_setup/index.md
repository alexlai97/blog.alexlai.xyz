---
title: "Blog Site Setup"
date: 2019-08-11
lastmod: 2020-04-07
draft: false
keywords: []
description: "how to setup this blog"
tags: ["blog", "setup", "hugo"]
categories: ["tutorial"]
summary: "You can setup a blog website like this. It's not that hard."

---

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->

- [Host locally](#host-locally)
    - [hugo setup](#hugo-setup)
    - [Get theme even](#get-theme-even)
    - [Copy config.toml and posts from exampleSite](#copy-configtoml-and-posts-from-examplesite)
- [Host remotely](#host-remotely)
    - [git push](#git-push)
    - [push to netlify](#push-to-netlify)
- [Host under your domain name](#host-under-your-domain-name)

<!-- markdown-toc end -->



# Host locally
## hugo setup
Get [hugo](https://gohugo.io/) here or use your package manager to install.

Then let's make a empty site.
```sh
$ hugo new site my_site
$ cd my_site 
$ git init
```

## Get theme even
hugo-theme-even [github](https://github.com/olOwOlo/hugo-theme-even) 

```sh
$ git submodule add https://github.com/olOwOlo/hugo-theme-even themes/even
```

## Copy config.toml and posts from exampleSite

```sh
$ cp -r themes/even/exampleSite/* .
```

Let's try it out locally.
```sh
$ hugo serve
```
Your website is serving at the local address (default [http://localhost:1313/](http://localhost:1313/)) mentioned in the output from terminal above. 

Now you can play around by changing files. Immediate changes will be shown in your browser.

# Host remotely
## git push
Go to your git server (gitlab or github or whatever you like), push it there.

## push to netlify
I am using [netlify](https://www.netlify.com/) as a CI tool and web server.

Put below in `my_site/netlify.toml`
```toml
 netlify.toml
[build]
  publish = "public"
  command = "hugo --gc --minify"

[context.production.environment]
  HUGO_VERSION = "0.68.3"
  HUGO_ENV = "production"
  HUGO_ENABLEGITINFO = "true"
```

Go to [netlify](https://www.netlify.com/), register and then link to your git repostiory. 
Then you should be given a domain (you can change it) that hosts your website!

Alternatively, you can use [github pages](https://pages.github.com/) with [travis-ci](https://travis-ci.com/) or [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) with [gitlab CI](https://docs.gitlab.com/ee/ci/README.html).

# Host under your domain name
First, you need to have a domain name. I bought one from [hover](https://www.hover.com/) for slightly above 10 bucks per year. You can buy it elsewhere.

I also uses [cloudflare](https://www.cloudflare.com/) as domain name server (DNS) instead of the default hover one. 

Finally, go to your Domain Name System (DNS) settings and add a CNAME record from your domain pointing to your website. The image below is my setup on cloudflare.

![cloudflare DNS setting](./cloudflare_DNS_setting_screenshot.png "alex's lai's cloudflare DNS setting")

Be patient and wait for DNS to propagate. 

If you have questions, ask your search engine first.

If you still have questions, send me an email.
