---
title: "About Me"
path: "about"

---

I am an 4B undergraduate student, studying CS major in University of Waterloo. Going to graduate in April 2020.

If you have any questions, email me [alexlai97@outlook.com](mailto:alexlai97@outlook.com).
Send me encrypted messages using my [public key](https://keybase.io/alexlai97) 
if you like (attach your public key so I can reply in the same manner).


Here are my other websites if you are interested.
- homepage: <https://alexlai.xyz>
- not-technical personal blog <https://personal.alexlai.xyz/>
